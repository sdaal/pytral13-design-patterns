import time
import logging
from functools import wraps

LARGE_NUMBER = 4 ** 39

logging.basicConfig(level=logging.WARNING)


def time_execution(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        t0 = time.perf_counter()
        result = func(*args, **kwargs)
        t1 = time.perf_counter()
        logging.debug("%s Took %.4f seconds", func.__name__, t1 - t0)
        return result
    return wrapper


@time_execution
def expensive_calculation(small_number):
    """Calculates a product a million times"""
    for _ in range(1_000_000):
        _ = LARGE_NUMBER * small_number + small_number ** 39
# expensive_calculation = time_execution(expensive_calculation)

if __name__ == '__main__':
    expensive_calculation(12)
