import sys


class Operational:
    def display_screen(self):
        return (
            "WELCOME to AB ATM\n"
            "- Get Cash\n"
            "- Check balance\n"
        )


class UnderMaintenance:
    def display_screen(self):
        return "UNDER MAINTENANCE! Cannot get money!!!"


class ATM:
    def __init__(self, address, mode=Operational):
        self.address = address
        self.mode = mode()
    
    def display(self):
        return self.mode.display_screen()


def get_mode(mode_name):
    if mode_name == "operational":
        return Operational
    return UnderMaintenance


if __name__ == '__main__':
    mode_name = "operational"
    if len(sys.argv) > 1:
        mode_name = sys.argv[1]
    mode = get_mode(mode_name)
    atm = ATM("myslym shyri", mode=mode)
    print(atm.display())
    print(sys.argv)
