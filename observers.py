import time
import random


class BitcoinPriceTracker:
    def __init__(self, price):
        self.price = price
        self._subs = []

    def add_subscriber(self, sub):
        self._subs.append(sub)

    def change_price(self, price):
        if price == self.price:
            return

        self.price = price
        for sub in self._subs:
            sub.notify(self.price)


class SlackChannel:
    def __init__(self, channel_name):
        self.channel_name = channel_name

    def notify(self, price):
        print(f"Hello {self.channel_name}, bitcoin is at {price}")


class SmsSender:
    def __init__(self, telno):
        self.telno = telno

    def notify(self, price):
        print(f"Hi, {self.telno}, bitcoin is at {price}")


if __name__ == '__main__':
    tracker = BitcoinPriceTracker(25_000)
    tracker.add_subscriber(SlackChannel("#students-only"))
    tracker.add_subscriber(SmsSender("+355652012345"))

    iteration = 0
    while True:
        tracker.change_price(tracker.price + random.randrange(-50, 50))
        iteration += 1
        time.sleep(1)
        if iteration == 60:
            break
