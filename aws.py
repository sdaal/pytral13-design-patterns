class S3Service:
    def __init__(self, username, password):
        self.username = username
        self.password = password

    def upload(self, filename):
        print(f"uploading {filename} ...")

    def download(self, filename):
        print(f"downloading {filename} ...")
