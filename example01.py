"""an example to learn about pylint"""
import time


LARGE_NUMBER = 4**39


class TimerDecorator:
    """A decorator to time the execution"""
    def __init__(self, func):
        self.times = []
        self.func = func

    def __call__(self, *args, **kwargs):
        start_time = time.time()
        result = self.func(*args, **kwargs)
        delta_time = time.time() - start_time
        self.times.append(delta_time)
        print(f"Function {self.func.__name__} has been executed in {delta_time}s "
              f"with the following arguments: {args} {kwargs}")
        return result


@TimerDecorator
def calculate_product_million_times(small_number):
    """Calculates a product a million times"""
    for _ in range(1_000_000):
        _ = LARGE_NUMBER * small_number + small_number ** 39

if __name__ == """__main__""":
    calculate_product_million_times(10)
