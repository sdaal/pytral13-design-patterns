import os.path
import datetime

import requests


class StorageStatusChecker:
    @classmethod
    def check_storage_status(cls, storage_dir, status_file):
        filepath = os.path.join(storage_dir, status_file)
        with open(filepath, 'r', encoding='utf-8') as f:
            content = f.read()

        code = int(content.strip())
        return code == 1


class UpstreamStatusChecker:
    @classmethod
    def check_upstream(cls, url):
        resp = requests.get(url)
        return 200 <= resp.status_code <= 299


class TimeChecker:
    @classmethod
    def check_second_is_even(cls):
        t = datetime.datetime.now()
        return t.second % 2 == 0


class StorageCheckAdapter:
    def __init__(self, storage_dir, filename):
        self.storage_dir = storage_dir
        self.filename = filename

    def check(self):
        return StorageStatusChecker.check_storage_status(
            self.storage_dir,
            self.filename,
        )


class UpstreamCheckAdapter:
    def __init__(self, url):
        self.url = url

    def check(self):
        return UpstreamStatusChecker.check_upstream(self.url)


class TimeCheckAdapter:
    def check(self):
        return TimeChecker.check_second_is_even()


class SystemStatusChecker:
    def __init__(self, systems_under_monitoring):
        self.systems = systems_under_monitoring

    def check(self, system):
        is_up = system.check()
        return '✓' if is_up else '✗'


if __name__ == '__main__':
    systems_under_monitoring = {
        "storage": StorageCheckAdapter("storage", "status.txt"),
        "upstream": UpstreamCheckAdapter("https://top-channel.tv"),
        "time": TimeCheckAdapter(),
    }
    status_checker = SystemStatusChecker(systems_under_monitoring)
    for name, system in status_checker.systems.items():
        print(f"{name} - {status_checker.check(system)}")
