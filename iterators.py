class ZeroToN:
    def __init__(self, n):
        self.n = n

    def __iter__(self):
        self.i = -1
        return self

    def __next__(self):
        self.i += 1
        if self.i < self.n:
            return self.i
        else:
            raise StopIteration



if __name__ == '__main__':
    it = iter(ZeroToN(10))
    while True:
        try:
            elem = next(it)
            print(elem)
        except StopIteration:
            print("DONE!")
            break
    
    print("\n------\n")
    for i in ZeroToN(10):
        print(i)
