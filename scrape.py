import requests
import lxml.html as et


def main():
    resp = requests.get('https://www.bankofalbania.org/Tregjet/Kursi_zyrtar_i_kembimit/')
    tree = et.fromstring(resp.content)
    rows = tree.xpath('//*[@id="middleColumn"]/div[2]/div/table[1]/tr')
    for row in rows:
        cells = row.findall('td')
        currency = cells[1].text_content()
        rate = cells[2].text_content()
        print(f'{currency} - {rate}')


if __name__ == '__main__':
    main()
