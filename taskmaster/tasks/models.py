from django.db import models
from django.utils.translation import gettext_lazy as _


class Task(models.Model):
    title = models.CharField(_("title"), max_length=100)
    is_done = models.BooleanField(_("is done?"), default=False)

    def __str__(self):
        mark = '✓' if self.is_done else '✗'
        return f'{self.title}: {mark}'
