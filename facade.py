class TwitterClient:
    def __init__(self, username, password):
        self.username = username
        self.password = password

    def login(self):
        print("logging in")
        print("sending username and password")

    def post(self, text):
        print(f"sending {text!r} to twitter ...")


class InstagramClient:
    def __init__(self, account):
        self.account = account

    def signin(self):
        print(f"signing in {self.account}")

    def upload_picture(self, pic):
        print(f"uploading {pic} for {self.account} ...")


class LinkedInClient:
    def __init__(self, username, password):
        self.username = username
        self.password = password

    def enter(self):
        print(f"entering linkedin for {self.username}")

    def post(self, text):
        print(f"posting on linkedin {text!r}")


class Scheduler:
    def __init__(self):
        self.twitter = TwitterClient("john", "pass123")
        self.instagram = InstagramClient("john")
        self.linkedin = LinkedInClient("john", "123456")

    def post(self):
        self.twitter.login()
        self.twitter.post("ulje te henen")

        self.instagram.signin()
        self.instagram.upload_picture("uljet si poster")

        self.linkedin.enter()
        self.linkedin.post("ulje te henen")


if __name__ == '__main__':
    Scheduler().post()
