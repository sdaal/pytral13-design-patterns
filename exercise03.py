import datetime


def calculate_age(birthdate):
    today = datetime.date.today()
    diff = today - birthdate
    age = diff.days / 365.25
    return age


class Student:
    def __init__(self, name, gpa):
        self.name = name
        self.gpa = gpa
