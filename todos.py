class LanguageEnglish:
    def __init__(self):
        self.lookups = {
            "enter_task": "Enter task> ",
            "done_question": "Which task you have done? ",
            "task_no_err": "Invalid task number",
            "no_tasks": "\tNo tasks todo!",
            "your_tasks": "Your tasks:",
            "show_question": "Show only in progress (Y|N)> ",
            "menu_question": "What do you want to do> ",
            "menu": """(L) list tasks
(A) add task
(M) mark task as done
(Q) quit app
""",
            "bye": "Bye bye!",
            "invalid": "Invalid Choice!!!",
        }

    def translate(self, placeholder):
        return self.lookups[placeholder]


class LanguageAlbanian:
    def __init__(self):
        self.lookups = {
            "enter_task": "Vendos taskun> ",
            "done_question": "Cfare tasku ke bere? ",
            "task_no_err": "Numer pa lidhje tasku",
            "no_tasks": "\tS'ke per te bere gje sot!",
            "your_tasks": "Gjerat qe ke per te bere:",
            "show_question": "Trego vetem tasqet ne progres (Y|N)> ",
            "menu_question": "Cfare do te besh> ",
            "menu": """(L) trego tasqet
(A) shto nje gje
(M) me thuaj ca ke bere
(Q) dil
""",
            "bye": "shnet!",
            "invalid": "Zgjedhje e gabuar!!!",
        }

    def translate(self, placeholder):
        return self.lookups[placeholder]


class Task:
    def __init__(self, title, is_done=False):
        self.title = title
        self.is_done = is_done

    def __str__(self):
        mark = '✓' if self.is_done else '✗'
        return f'{self.title}: {mark}'

    def mark_done(self):
        self.is_done = True


class App:
    def __init__(self):
        self.tasks = []
        self.lang = self.make_language("en")

    def set_language(self):
        print("Available languages:")
        print("\ten-US: English")
        print("\tsq-AL: Albanian")
        language = input("Pick language> ").lower()
        print(f"Setting language to {language}")
        self.lang = self.make_language(language)

    def make_language(self, lang_code):
        if lang_code.startswith('sq'):
            return LanguageAlbanian()
        else:
            return LanguageEnglish()

    def menu(self):
        print(self.lang.translate("menu"))
        choice = input(self.lang.translate("menu_question")).upper()
        return choice

    def add_task(self):
        title = input(self.lang.translate("enter_task"))
        self.tasks.append(Task(title))

    def mark_as_done(self):
        self.show()
        num = input(self.lang.translate("done_question"))
        try:
            task_no = int(num) - 1
            self.tasks[task_no].mark_done()
        except (ValueError, IndexError):
            print(self.lang.translate("task_no_err"))
            return

    def show(self):
        if not self.tasks:
            print(self.lang.translate("no_tasks"))
            return

        show_type = input(self.lang.translate("show_question")).upper()
        print(self.lang.translate("your_tasks"))
        for i, task in enumerate(self.tasks, 1):
            if show_type == 'Y' and task.is_done:
                continue
            print(f"\t{i}. {task}")

    def run(self):
        while True:
            choice = self.menu()

            if choice == 'L':
                self.show()
            elif choice == 'A':
                self.add_task()
            elif choice == 'M':
                self.mark_as_done()
            elif choice == 'Q':
                print(self.lang.translate("bye"))
                break
            else:
                print(self.lang.translate("invalid"))
                continue



def main():
    app = App()
    app.set_language()
    app.run()


if __name__ == '__main__':
    main()
