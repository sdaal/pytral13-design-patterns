class Computer:
    def __init__(self, cpu, ram):
        self.cpu = cpu
        self.ram = ram
        self.gpu = None
        self.bluetooth = False
        self.fingerprint = False
        self.webcam = False
        self.hdd = True
        self.ssd = False
        self.display = 'FHD'
        self.keyboard_layout = 'en-US'

    def __str__(self):
        kv_pairs = []
        for attr, val in self.__dict__.items():
            kv_pairs.append(f'{attr}={val!r}')
        return '\n'.join(kv_pairs)


class ComputerBuilder:
    def __init__(self, cpu, ram):
        self.computer = Computer(cpu, ram)

    def with_gpu(self, gpu='RTX 3050'):
        self.computer.gpu = gpu
        return self

    def with_bluetooth(self):
        self.computer.bluetooth = True
        return self

    def with_fingerprint_reader(self):
        self.computer.fingerprint = True
        return self
    
    def with_webcam(self):
        self.computer.webcam = True
        return self

    def with_ssd(self):
        self.computer.hdd = False
        self.computer.ssd = True
        return self

    def with_display(self, display='UHD'):
        self.computer.display = display
        return self

    def with_kb_layout(self, layout='en-UK'):
        self.computer.keyboard_layout = layout
        return self

    def build(self):
        return self.computer
