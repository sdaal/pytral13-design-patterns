from curses import A_HORIZONTAL
from aws import S3Service
from azure import BlobStorage


class StorageService:
    def get(self, name):
        pass

    def put(self, name):
        pass


class S3StorageService:
    def __init__(self):
        self.s3 = S3Service('a', 'b')

    def get(self, name):
        self.s3.download(name)

    def put(self, name):
        self.s3.upload(name)


class AzureStorageService:
    def __init__(self):
        self.abs = BlobStorage('1213131b')

    def get(self, name):
        self.abs.get(name)

    def put(self, name):
        self.abs.save(name)


def get_storage_service(provider="aws"):
    if provider == 'aws':
        return S3StorageService()
    else:
        return AzureStorageService()


class Profile:
    def __init__(self, username):
        self.username = username

    def add_profile_picture(self, photo):
        storage = get_storage_service()
        storage.put(photo)
