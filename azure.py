class BlobStorage:
    def __init__(self, key):
        self.key = key

    def save(self, name):
        print(f"saving file named: {name}!")

    def get(self, name):
        print(f"getting {name}!")
