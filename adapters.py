class Pillow:
    @classmethod
    def grayscale(cls, photo, intensity=0.8):
        return f'-{photo}-'
    
    @classmethod
    def mexico(cls, photo, yellowness=75):
        return f"^{photo}^"


class PgMagick:
    @classmethod
    def warm(cls, photo, temperature, altitude):
        return f"={photo}="
    
    @classmethod
    def cold(cls, photo, ac, ed, xy):
        return f"_{photo}_"


class Cv2:
    @classmethod
    def chalk(cls, photo, bold, color, bg):
        return f"\\{photo}\/"


class GrayscaleFilter:
    @classmethod
    def apply(cls, photo):
        return Pillow.grayscale(photo, 0.75)


class MexicoFilter:
    @classmethod
    def apply(cls, photo):
        return Pillow.mexico(photo, 100)


class WarmFilter:
    @classmethod
    def apply(cls, photo):
        return PgMagick.warm(photo, 25, 500)


class ColdFilter:
    @classmethod
    def apply(cls, photo):
        return PgMagick.cold(photo, 10, 12, 24)


class ChalkFilter:
    @classmethod
    def apply(cls, photo):
        return Cv2.chalk(photo, True, (128, 64, 64), (255, 128, 0))


class InstagramAccount:
    def __init__(self, user):
        self.user = user
        self.pics = []

    def upload_photo(self, pic, flt):
        pic = flt.apply(pic)
        self.pics.append(pic)

    def show_gallery(self):
        print(f"User: {self.user}")
        for pic in self.pics:
            print(f"|> {pic} <|")


if __name__ == '__main__':
    acc = InstagramAccount("nyzoo")
    acc.upload_photo("elephant", WarmFilter)
    acc.upload_photo("lion", WarmFilter)
    acc.upload_photo("zebra", ColdFilter)
    acc.upload_photo("tiger", ChalkFilter)
    acc.upload_photo("crocodile", MexicoFilter)

    acc.show_gallery()
