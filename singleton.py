class Student:
    _instance = None

    def __init__(self, name):
        print("initializing")
        self.name = name

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            print("called for the first time. creating instance.")
            cls._instance = object.__new__(cls)
        print("returning instance ...")
        return cls._instance
